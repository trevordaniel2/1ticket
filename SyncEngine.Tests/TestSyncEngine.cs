﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;

namespace SyncEngine.Tests
{
    [TestClass]
    public class TestSyncEngine
    {
        [TestMethod]
        public async Task SyncEngine_Sync()
        {
            await SyncEngine.Functions.Sync("21902679");
        }
    }
}
