﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyBox.Model.Ticket
{

    public class Rootobject
    {
        public string inHandDate { get; set; }
        public int id { get; set; }
        public int accountId { get; set; }
        public int eventId { get; set; }
        public int quantity { get; set; }
        public string notes { get; set; }
        public string section { get; set; }
        public string row { get; set; }
        public string secondRow { get; set; }
        public int lowSeat { get; set; }
        public int highSeat { get; set; }
        public float cost { get; set; }
        public float faceValue { get; set; }
        public List<Ticket> tickets { get; set; }
        public List<int> ticketIds { get; set; }
        public string stockType { get; set; }
        public string splitType { get; set; }
        public string customSplit { get; set; }
        public float? listPrice { get; set; }
        public float? expectedValue { get; set; }
        public string publicNotes { get; set; }
        public List<string> attributes { get; set; }
        public string status { get; set; }
        public int? inHandDaysBeforeEvent { get; set; }
        public DateTime lastPriceUpdate { get; set; }
        public string createdBy { get; set; }
        public string lastUpdateBy { get; set; }
        public long version { get; set; }
        public string tags { get; set; }
        public string seatType { get; set; }
        public object eventMapping { get; set; }
        public int mappingId { get; set; }
        public int exchangePosId { get; set; }
        public bool broadcast { get; set; }
        public bool zoneSeating { get; set; }
        public bool electronicTransfer { get; set; }
        public bool optOutAutoPrice { get; set; }
        public bool hideSeatNumbers { get; set; }
        public string vsrOption { get; set; }
        public int? replenishmentGroupId { get; set; }
    }

    public class Ticket
    {
        public int id { get; set; }
        public int seatNumber { get; set; }
        public string fileName { get; set; }
        public string barCode { get; set; }
        public int inventoryId { get; set; }
        public int invoiceLineId { get; set; }
        public int purchaseLineId { get; set; }
        public string section { get; set; }
        public string row { get; set; }
        public string notes { get; set; }
        public float cost { get; set; }
        public float faceValue { get; set; }
        public float sellPrice { get; set; }
        public string stockType { get; set; }
        public int eventId { get; set; }
        public int accountId { get; set; }
        public string status { get; set; }
        public string base64FileBytes { get; set; }
        public List<string> disclosures { get; set; }
        public List<string> attributes { get; set; }
        public DateTime createdDate { get; set; }
        public string createdBy { get; set; }
        public DateTime lastUpdate { get; set; }
        public string lastUpdateBy { get; set; }
    }

}
