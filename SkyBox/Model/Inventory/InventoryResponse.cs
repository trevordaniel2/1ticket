﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyBox.Model.Inventory
{
    public class Rootobject
    {
        public List<Row> rows { get; set; }
        public int rowCount { get; set; }
        public object totals { get; set; }
    }

    public class Row
    {
        public string inHandDate { get; set; }
        public int id { get; set; }
        public int accountId { get; set; }
        public int eventId { get; set; }
        public int quantity { get; set; }
        public string notes { get; set; }
        public string section { get; set; }
        public string row { get; set; }
        public string secondRow { get; set; }
        public int lowSeat { get; set; }
        public int highSeat { get; set; }
        public float cost { get; set; }
        public float faceValue { get; set; }
        public List<Ticket> tickets { get; set; }
        public List<int> ticketIds { get; set; }
        public string stockType { get; set; }
        public string splitType { get; set; }
        public string customSplit { get; set; }
        public float? listPrice { get; set; }
        public float? expectedValue { get; set; }
        public string publicNotes { get; set; }
        public List<string> attributes { get; set; }
        public string status { get; set; }
        public int? inHandDaysBeforeEvent { get; set; }
        public DateTime lastPriceUpdate { get; set; }
        public string createdBy { get; set; }
        public string lastUpdateBy { get; set; }
        public long version { get; set; }
        public string tags { get; set; }
        public string seatType { get; set; }
        public object eventMapping { get; set; }
        public int mappingId { get; set; }
        public int exchangePosId { get; set; }
        public bool broadcast { get; set; }
        public bool zoneSeating { get; set; }
        public bool electronicTransfer { get; set; }
        public bool optOutAutoPrice { get; set; }
        public bool hideSeatNumbers { get; set; }
        public string vsrOption { get; set; }
        public int? replenishmentGroupId { get; set; }
        public string seatNumbers { get; set; }
        public bool filesUploaded { get; set; }
        public bool barCodesEntered { get; set; }
        public bool listed { get; set; }
        public int? holdId { get; set; }
        public Hold hold { get; set; }
        public float unitCostAverage { get; set; }
        public List<string> currencies { get; set; }
        public string received { get; set; }
        public List<int> vendorId { get; set; }
        public Lastinventorypriceupdate lastInventoryPriceUpdate { get; set; }
        public bool inHand { get; set; }
        public Event _event { get; set; }
    }

    public class Hold
    {
        public int id { get; set; }
        public int accountId { get; set; }
        public int inventoryId { get; set; }
        public string user { get; set; }
        public int userId { get; set; }
        public int removedByUserId { get; set; }
        public DateTime createdDate { get; set; }
        public DateTime expiryDate { get; set; }
        public int customerId { get; set; }
        public object removedDate { get; set; }
        public string notes { get; set; }
        public object externalRef { get; set; }
        public float listPrice { get; set; }
    }

    public class Lastinventorypriceupdate
    {
        public int id { get; set; }
        public int accountId { get; set; }
        public int inventoryId { get; set; }
        public string email { get; set; }
        public int userId { get; set; }
        public DateTime createdDate { get; set; }
        public string section { get; set; }
        public string row { get; set; }
        public object secondRow { get; set; }
        public string seats { get; set; }
        public int quantity { get; set; }
        public float previousListPrice { get; set; }
        public float listPrice { get; set; }
    }

    public class Event
    {
        public int id { get; set; }
        public string name { get; set; }
        public DateTime date { get; set; }
        public Venue venue { get; set; }
        public int performerId { get; set; }
        public Performer performer { get; set; }
        public string keywords { get; set; }
        public int stubhubEventId { get; set; }
        public string stubhubEventUrl { get; set; }
        public object tags { get; set; }
        public object notes { get; set; }
        public int eiEventId { get; set; }
        public bool optOutReplenishment { get; set; }
        public object ticketCount { get; set; }
        public string vividSeatsEventUrl { get; set; }
    }

    public class Venue
    {
        public int id { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string country { get; set; }
        public string postalCode { get; set; }
        public string phone { get; set; }
        public string timeZone { get; set; }
    }

    public class Performer
    {
        public int id { get; set; }
        public string name { get; set; }
        public string eventType { get; set; }
        public Category category { get; set; }
    }

    public class Category
    {
        public int id { get; set; }
        public string name { get; set; }
        public string eventType { get; set; }
    }

    public class Ticket
    {
        public int id { get; set; }
        public int seatNumber { get; set; }
        public string fileName { get; set; }
        public string barCode { get; set; }
        public int inventoryId { get; set; }
        public int invoiceLineId { get; set; }
        public int purchaseLineId { get; set; }
        public string section { get; set; }
        public string row { get; set; }
        public string notes { get; set; }
        public float cost { get; set; }
        public float faceValue { get; set; }
        public float sellPrice { get; set; }
        public string stockType { get; set; }
        public int eventId { get; set; }
        public int accountId { get; set; }
        public string status { get; set; }
        public string base64FileBytes { get; set; }
        public List<string> disclosures { get; set; }
        public List<string> attributes { get; set; }
        public DateTime createdDate { get; set; }
        public string createdBy { get; set; }
        public DateTime lastUpdate { get; set; }
        public string lastUpdateBy { get; set; }
    }

}
