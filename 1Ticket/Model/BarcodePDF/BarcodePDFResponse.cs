﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1Ticket.Model.BarcodePDF
{
    // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
    /// <remarks/>
    /// 
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class seats
    {

        private List<seatsSeat> seatField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("seat")]
        public List<seatsSeat> seat
        {
            get
            {
                return this.seatField;
            }
            set
            {
                this.seatField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class seatsSeat
    {

        private string seatidField;

        private string robojidField;

        private string seatnumberField;

        private string sectionField;

        private string rowField;

        private string pdfguidField;

        private string barcodeField;

        private string pdfurlField;

        /// <remarks/>
        public string seatid
        {
            get
            {
                return this.seatidField;
            }
            set
            {
                this.seatidField = value;
            }
        }

        /// <remarks/>
        public string robojid
        {
            get
            {
                return this.robojidField;
            }
            set
            {
                this.robojidField = value;
            }
        }

        /// <remarks/>
        public string seatnumber
        {
            get
            {
                return this.seatnumberField;
            }
            set
            {
                this.seatnumberField = value;
            }
        }

        /// <remarks/>
        public string section
        {
            get
            {
                return this.sectionField;
            }
            set
            {
                this.sectionField = value;
            }
        }

        /// <remarks/>
        public string row
        {
            get
            {
                return this.rowField;
            }
            set
            {
                this.rowField = value;
            }
        }

        /// <remarks/>
        public string pdfguid
        {
            get
            {
                return this.pdfguidField;
            }
            set
            {
                this.pdfguidField = value;
            }
        }

        /// <remarks/>
        public string barcode
        {
            get
            {
                return this.barcodeField;
            }
            set
            {
                this.barcodeField = value;
            }
        }

        /// <remarks/>
        public string pdfurl
        {
            get
            {
                return this.pdfurlField;
            }
            set
            {
                this.pdfurlField = value;
            }
        }
    }


}
