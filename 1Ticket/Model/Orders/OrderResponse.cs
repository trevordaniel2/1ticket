﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1Ticket.Model.Orders
{

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class roboj
    {

        private robojGetorders getordersField;

        /// <remarks/>
        public robojGetorders getorders
        {
            get
            {
                return this.getordersField;
            }
            set
            {
                this.getordersField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class robojGetorders
    {

        private robojGetordersPagination paginationField;

        private List<robojGetordersOrder> orderField;

        /// <remarks/>
        public robojGetordersPagination pagination
        {
            get
            {
                return this.paginationField;
            }
            set
            {
                this.paginationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("order")]
        public List<robojGetordersOrder> order
        {
            get
            {
                return this.orderField;
            }
            set
            {
                this.orderField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class robojGetordersPagination
    {

        private byte totalpagesField;

        private byte currentpageField;

        private ushort resultscurrentpageField;

        private ushort resultstotalField;

        /// <remarks/>
        public byte totalpages
        {
            get
            {
                return this.totalpagesField;
            }
            set
            {
                this.totalpagesField = value;
            }
        }

        /// <remarks/>
        public byte currentpage
        {
            get
            {
                return this.currentpageField;
            }
            set
            {
                this.currentpageField = value;
            }
        }

        /// <remarks/>
        public ushort resultscurrentpage
        {
            get
            {
                return this.resultscurrentpageField;
            }
            set
            {
                this.resultscurrentpageField = value;
            }
        }

        /// <remarks/>
        public ushort resultstotal
        {
            get
            {
                return this.resultstotalField;
            }
            set
            {
                this.resultstotalField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class robojGetordersOrder
    {

        private string statusField;

        private robojGetordersOrderData_event data_eventField;

        private robojGetordersOrderData_pdf data_pdfField;

        private robojGetordersOrderData_tickets data_ticketsField;

        private robojGetordersOrderData_purchase data_purchaseField;

        private robojGetordersOrderData_account data_accountField;

        private List<robojGetordersOrderData_pos> data_posField;

        private robojGetordersOrderData_crawl data_crawlField;

        private uint idField;

        /// <remarks/>
        public string status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>
        public robojGetordersOrderData_event data_event
        {
            get
            {
                return this.data_eventField;
            }
            set
            {
                this.data_eventField = value;
            }
        }

        /// <remarks/>
        public robojGetordersOrderData_pdf data_pdf
        {
            get
            {
                return this.data_pdfField;
            }
            set
            {
                this.data_pdfField = value;
            }
        }

        /// <remarks/>
        public robojGetordersOrderData_tickets data_tickets
        {
            get
            {
                return this.data_ticketsField;
            }
            set
            {
                this.data_ticketsField = value;
            }
        }

        /// <remarks/>
        public robojGetordersOrderData_purchase data_purchase
        {
            get
            {
                return this.data_purchaseField;
            }
            set
            {
                this.data_purchaseField = value;
            }
        }

        /// <remarks/>
        public robojGetordersOrderData_account data_account
        {
            get
            {
                return this.data_accountField;
            }
            set
            {
                this.data_accountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("data_pos")]
        public List<robojGetordersOrderData_pos> data_pos
        {
            get
            {
                return this.data_posField;
            }
            set
            {
                this.data_posField = value;
            }
        }

        /// <remarks/>
        public robojGetordersOrderData_crawl data_crawl
        {
            get
            {
                return this.data_crawlField;
            }
            set
            {
                this.data_crawlField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public uint id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class robojGetordersOrderData_event
    {

        private string eventnameField;

        private string eventdateField;

        private string eventtimeField;

        private string venuenameField;

        private string eventid_ticketnetworkField;

        private string eventid_tickettechnologyField;

        private string print_delay_hoursField;

        /// <remarks/>
        public string eventname
        {
            get
            {
                return this.eventnameField;
            }
            set
            {
                this.eventnameField = value;
            }
        }

        /// <remarks/>
        public string eventdate
        {
            get
            {
                return this.eventdateField;
            }
            set
            {
                this.eventdateField = value;
            }
        }

        /// <remarks/>
        public string eventtime
        {
            get
            {
                return this.eventtimeField;
            }
            set
            {
                this.eventtimeField = value;
            }
        }

        /// <remarks/>
        public string venuename
        {
            get
            {
                return this.venuenameField;
            }
            set
            {
                this.venuenameField = value;
            }
        }

        /// <remarks/>
        public string eventid_ticketnetwork
        {
            get
            {
                return this.eventid_ticketnetworkField;
            }
            set
            {
                this.eventid_ticketnetworkField = value;
            }
        }

        /// <remarks/>
        public string eventid_tickettechnology
        {
            get
            {
                return this.eventid_tickettechnologyField;
            }
            set
            {
                this.eventid_tickettechnologyField = value;
            }
        }

        /// <remarks/>
        public string print_delay_hours
        {
            get
            {
                return this.print_delay_hoursField;
            }
            set
            {
                this.print_delay_hoursField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class robojGetordersOrderData_pdf
    {

        private bool existsField;

        /// <remarks/>
        public bool exists
        {
            get
            {
                return this.existsField;
            }
            set
            {
                this.existsField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class robojGetordersOrderData_tickets
    {

        private string sectionField;

        private string rowField;

        private byte quantityField;

        private string seatfirstField;

        private string seatlastField;

        private string ticketstockField;

        private string obstructionsField;

        /// <remarks/>
        public string section
        {
            get
            {
                return this.sectionField;
            }
            set
            {
                this.sectionField = value;
            }
        }

        /// <remarks/>
        public string row
        {
            get
            {
                return this.rowField;
            }
            set
            {
                this.rowField = value;
            }
        }

        /// <remarks/>
        public byte quantity
        {
            get
            {
                return this.quantityField;
            }
            set
            {
                this.quantityField = value;
            }
        }

        /// <remarks/>
        public string seatfirst
        {
            get
            {
                return this.seatfirstField;
            }
            set
            {
                this.seatfirstField = value;
            }
        }

        /// <remarks/>
        public string seatlast
        {
            get
            {
                return this.seatlastField;
            }
            set
            {
                this.seatlastField = value;
            }
        }

        /// <remarks/>
        public string ticketstock
        {
            get
            {
                return this.ticketstockField;
            }
            set
            {
                this.ticketstockField = value;
            }
        }

        /// <remarks/>
        public string obstructions
        {
            get
            {
                return this.obstructionsField;
            }
            set
            {
                this.obstructionsField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class robojGetordersOrderData_purchase
    {

        private string confnumField;

        private object confnum_originalField;

        private string confregionField;

        private string dateField;

        private string usernameField;

        private string purchasernameField;

        private string vendorField;

        private string creditcardField;

        private string totalcostField;

        private robojGetordersOrderData_purchaseCost_details cost_detailsField;

        private string last_updatedField;

        /// <remarks/>
        public string confnum
        {
            get
            {
                return this.confnumField;
            }
            set
            {
                this.confnumField = value;
            }
        }

        /// <remarks/>
        public object confnum_original
        {
            get
            {
                return this.confnum_originalField;
            }
            set
            {
                this.confnum_originalField = value;
            }
        }

        /// <remarks/>
        public string confregion
        {
            get
            {
                return this.confregionField;
            }
            set
            {
                this.confregionField = value;
            }
        }

        /// <remarks/>
        public string date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }

        /// <remarks/>
        public string username
        {
            get
            {
                return this.usernameField;
            }
            set
            {
                this.usernameField = value;
            }
        }

        /// <remarks/>
        public string purchasername
        {
            get
            {
                return this.purchasernameField;
            }
            set
            {
                this.purchasernameField = value;
            }
        }

        /// <remarks/>
        public string vendor
        {
            get
            {
                return this.vendorField;
            }
            set
            {
                this.vendorField = value;
            }
        }

        /// <remarks/>
        public string creditcard
        {
            get
            {
                return this.creditcardField;
            }
            set
            {
                this.creditcardField = value;
            }
        }

        /// <remarks/>
        public string totalcost
        {
            get
            {
                return this.totalcostField;
            }
            set
            {
                this.totalcostField = value;
            }
        }

        /// <remarks/>
        public robojGetordersOrderData_purchaseCost_details cost_details
        {
            get
            {
                return this.cost_detailsField;
            }
            set
            {
                this.cost_detailsField = value;
            }
        }

        /// <remarks/>
        public string last_updated
        {
            get
            {
                return this.last_updatedField;
            }
            set
            {
                this.last_updatedField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class robojGetordersOrderData_purchaseCost_details
    {

        private decimal cost_totalField;

        private decimal cost_perticket_faceField;

        private decimal cost_perticket_feeField;

        /// <remarks/>
        public decimal cost_total
        {
            get
            {
                return this.cost_totalField;
            }
            set
            {
                this.cost_totalField = value;
            }
        }

        /// <remarks/>
        public decimal cost_perticket_face
        {
            get
            {
                return this.cost_perticket_faceField;
            }
            set
            {
                this.cost_perticket_faceField = value;
            }
        }

        /// <remarks/>
        public decimal cost_perticket_fee
        {
            get
            {
                return this.cost_perticket_feeField;
            }
            set
            {
                this.cost_perticket_feeField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class robojGetordersOrderData_account
    {

        private ushort userid_1ticketField;

        private uint accountidField;

        private string noteField;

        private string customerid_ttField;

        private string customerid_tnField;

        private object creditcardid_ttField;

        private object creditccardid_tnField;

        /// <remarks/>
        public ushort userid_1ticket
        {
            get
            {
                return this.userid_1ticketField;
            }
            set
            {
                this.userid_1ticketField = value;
            }
        }

        /// <remarks/>
        public uint accountid
        {
            get
            {
                return this.accountidField;
            }
            set
            {
                this.accountidField = value;
            }
        }

        /// <remarks/>
        public string note
        {
            get
            {
                return this.noteField;
            }
            set
            {
                this.noteField = value;
            }
        }

        /// <remarks/>
        public string customerid_tt
        {
            get
            {
                return this.customerid_ttField;
            }
            set
            {
                this.customerid_ttField = value;
            }
        }

        /// <remarks/>
        public string customerid_tn
        {
            get
            {
                return this.customerid_tnField;
            }
            set
            {
                this.customerid_tnField = value;
            }
        }

        /// <remarks/>
        public object creditcardid_tt
        {
            get
            {
                return this.creditcardid_ttField;
            }
            set
            {
                this.creditcardid_ttField = value;
            }
        }

        /// <remarks/>
        public object creditccardid_tn
        {
            get
            {
                return this.creditccardid_tnField;
            }
            set
            {
                this.creditccardid_tnField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class robojGetordersOrderData_pos
    {

        private object purchase_orderField;

        private string postypeField;

        /// <remarks/>
        public object purchase_order
        {
            get
            {
                return this.purchase_orderField;
            }
            set
            {
                this.purchase_orderField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string postype
        {
            get
            {
                return this.postypeField;
            }
            set
            {
                this.postypeField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class robojGetordersOrderData_crawl
    {

        private string crawled_emailField;

        private string crawled_websiteField;

        /// <remarks/>
        public string crawled_email
        {
            get
            {
                return this.crawled_emailField;
            }
            set
            {
                this.crawled_emailField = value;
            }
        }

        /// <remarks/>
        public string crawled_website
        {
            get
            {
                return this.crawled_websiteField;
            }
            set
            {
                this.crawled_websiteField = value;
            }
        }
    }


}
