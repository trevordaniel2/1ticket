﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Serialization;

namespace _1Ticket
{
    public class Functions
    {
        public static async Task<Model.Orders.roboj> GetOrders(string username, string password, DateTime purchaseDateStart = default(DateTime), DateTime purchaseDateEnd = default(DateTime))
        {
            Model.Orders.roboj result = new Model.Orders.roboj();

            string url = "http://1ticket.com/api/searchOrders/?username=" + username + "&password=" + password;
            if (purchaseDateStart != null && purchaseDateEnd != null)
            {
                url = url + "&purchasedate=" + HttpUtility.UrlEncode(purchaseDateStart.ToString("MM/dd/yyyy")) + "&purchasedateend=" + HttpUtility.UrlEncode(purchaseDateEnd.ToString("MM/dd/yyyy"));
            }

            string xml = await Helpers.Functions.ExecuteJsonSkyBox(new Uri(url), new HttpMethod("GET"));

            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Model.Orders.roboj));
                MemoryStream memStream = new MemoryStream(Encoding.UTF8.GetBytes(xml));
                result = (Model.Orders.roboj)serializer.Deserialize(memStream);
            }
            catch
            {
                result = null;
            }

            return result;

        }
        public static async Task<Model.BarcodePDF.seats> GetBarcodePDFDetails(string username, string password, uint ticketId)
        {
            Model.BarcodePDF.seats result = new Model.BarcodePDF.seats();

            //bool gotResults = false;

            //while (gotResults == false)
            //{
                try
                {
                    string url = "http://1ticket.com/api/getPDF/v3/?username=" + username + "&password=" + password + "&1ticketid=" + ticketId;

                    string xml = await Helpers.Functions.ExecuteJsonSkyBox(new Uri(url), new HttpMethod("GET"));


                    XmlSerializer serializer = new XmlSerializer(typeof(_1Ticket.Model.BarcodePDF.seats));
                    MemoryStream memStream = new MemoryStream(Encoding.UTF8.GetBytes(xml));
                    result = (Model.BarcodePDF.seats)serializer.Deserialize(memStream);

                    //gotResults = true;
                }
                catch
                {
                    result = null;
                }
            //}

            return result;
        }
    }
}
