﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using System.Linq;

namespace _1Ticket.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public async Task Ticket_GetOrders()
        {
            uint problemTicketId = 14962349;

            _1Ticket.Model.Orders.roboj _1TickOrders = await _1Ticket.Functions.GetOrders("api%2B6718138216215%401ticket.com", "787171004", DateTime.Now.AddDays(-31), DateTime.Now);

            string confirmation = _1TickOrders.getorders.order.Where(x => x.id == problemTicketId).FirstOrDefault().data_purchase.confnum;
        }
        [TestMethod]
        public async Task Ticket_BarcodePDFDetails()
        {
            _1Ticket.Model.BarcodePDF.seats barcodePDFDetails = await _1Ticket.Functions.GetBarcodePDFDetails("api%2B6718138216215%401ticket.com", "787171004", 14903715);
        }
    }
}
