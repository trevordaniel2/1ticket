﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.IO;

namespace SkyBox.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public async Task SkyBox_TestAPI()
        {
            string result = await Helpers.Functions.ExecuteJsonSkyBox(new Uri("https://skybox.vividseats.com:443/services/venues"), new HttpMethod("GET"));
        }
        [TestMethod]
        public async Task SkyBox_GetInventory()
        {
            string result = await Helpers.Functions.ExecuteJsonSkyBox(new Uri("https://skybox.vividseats.com:443/services/inventory"), new HttpMethod("GET"));

            SkyBox.Model.Inventory.Rootobject tmp = JsonConvert.DeserializeObject<SkyBox.Model.Inventory.Rootobject>(result);

            List<Model.Inventory.Row> rows = tmp.rows.Where(x => x.notes.Contains("41-40989")).ToList();

        }
        [TestMethod]
        public async Task SkyBox_GetInventoryDelta()
        {

            string startTime = HttpUtility.UrlEncode(DateTime.Now.AddMinutes(-10).ToString("yyyy-MM-ddTHH:mm:ssZ"));
            string endTime = HttpUtility.UrlEncode(DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ssZ"));

            string result = await Helpers.Functions.ExecuteJsonSkyBox(new Uri("https://skybox.vividseats.com:443/services/inventory/delta?startTime=" + startTime + "&endTime=" + endTime), new HttpMethod("GET"));

            List<string> tmp = JsonConvert.DeserializeObject<List<string>>(result);

            // get the first ticket
            string tickectJSON = await Helpers.Functions.ExecuteJsonSkyBox(new Uri("https://skybox.vividseats.com:443/services/inventory/" + tmp.FirstOrDefault() + "?includeTickets=true"), new HttpMethod("GET"));
            SkyBox.Model.Ticket.Rootobject skyBoxInvetoryItem = JsonConvert.DeserializeObject<SkyBox.Model.Ticket.Rootobject>(tickectJSON);
        }
        [TestMethod]
        public async Task SkyBox_UpdateInventoryItem()
        {
            // get the first ticket
            string tickectJSON = await Helpers.Functions.ExecuteJsonSkyBox(new Uri("https://skybox.vividseats.com:443/services/inventory/22781019?includeTickets=true"), new HttpMethod("GET"));
            SkyBox.Model.Ticket.Rootobject skyBoxInvetoryItem = JsonConvert.DeserializeObject<SkyBox.Model.Ticket.Rootobject>(tickectJSON);

            // change something on the ticket
            skyBoxInvetoryItem.tickets[0].barCode = "5678";

            // upload the PDF
            byte[] pdfAsBytes = await DownloadPDF("http://balance.1ticket.com/viewer.asp?id=%7B019E51BF-1AEC-447B-B5EF-5475D80A51B4%7D");
            skyBoxInvetoryItem.tickets[0].base64FileBytes = Convert.ToBase64String(pdfAsBytes);

            // update the ticket via the api
            string sendThis = JsonConvert.SerializeObject(skyBoxInvetoryItem);
            // update the item via the API
            await Helpers.Functions.ExecuteJsonSkyBox(new Uri("https://skybox.vividseats.com:443/services/inventory/" + skyBoxInvetoryItem.id), new HttpMethod("PUT"), sendThis);
        }
        private static async Task<byte[]> DownloadPDF(string pdfUrl)
        {
            TaskCompletionSource<byte[]> tcs = new TaskCompletionSource<byte[]>();
            HttpWebRequest request = WebRequest.CreateHttp(pdfUrl);
            using (HttpWebResponse response = (HttpWebResponse)(await request.GetResponseAsync()))
            using (Stream stream = response.GetResponseStream())
            using (MemoryStream ms = new MemoryStream())
            {
                await stream.CopyToAsync(ms);
                tcs.SetResult(ms.ToArray());
                return await tcs.Task;
            }
        }
    }
}
