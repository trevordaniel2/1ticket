﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Helpers
{
    public class Functions
    {
        private static readonly HttpClient _client = new HttpClient();

        public static async Task<string> ExecuteJsonSkyBox(Uri uri, HttpMethod method, string bodyContent = null)
        {
            HttpRequestMessage request = new HttpRequestMessage
            {
                RequestUri = uri,
                Method = method
            };

            if (bodyContent != null)
            {
                request.Content = new StringContent(bodyContent, Encoding.UTF8, "application/json");
            }

            request.Headers.Add("X-Api-Token", "52ddb50e-0f5a-41b2-9190-d7ba325b0bc1");
            request.Headers.Add("X-Account", "000");

            HttpResponseMessage response = await _client.SendAsync(request);
            return await response.Content.ReadAsStringAsync();
        }
    }
}
