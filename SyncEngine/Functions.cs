﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace SyncEngine
{
    public class Functions
    {
        private static _1Ticket.Model.Orders.roboj _1TickOrders = new _1Ticket.Model.Orders.roboj();
        private static List<SkyBox.Model.Inventory.Row> skyBoxInventory = new List<SkyBox.Model.Inventory.Row>();
        private static List<SkyBox.Model.Inventory.Row> skyBoxInventoryThatWasUpdated = new List<SkyBox.Model.Inventory.Row>();
        private static List<string> Messages = new List<string>();

        public static async Task Sync(string skyBoxId = null)
        {
            if (skyBoxId == null)
            {
                // get inventory from SkyBox
                string result = await Helpers.Functions.ExecuteJsonSkyBox(new Uri("https://skybox.vividseats.com:443/services/inventory?tags=qc%20dd&StockType=E-Tickets&includeTickets=true"), new HttpMethod("GET"));
                skyBoxInventory = JsonConvert.DeserializeObject<SkyBox.Model.Inventory.Rootobject>(result).rows;
            }
            else
            {
                // get a single item for testing from SkyBox
                string result = await Helpers.Functions.ExecuteJsonSkyBox(new Uri("https://skybox.vividseats.com:443/services/inventory/" + skyBoxId + "?includeTickets=true"), new HttpMethod("GET"));
                skyBoxInventory.Add(JsonConvert.DeserializeObject<SkyBox.Model.Inventory.Row>(result));
            }

            Messages.Add(skyBoxInventory.Count + " inventory items found in SkyBox");

            // remove everything that doesn't have a stock type of "ELECTRONIC" - which the above filter was supposed to do anyway....
            Messages.Add(skyBoxInventory.Where(x => x.stockType != "ELECTRONIC").ToList().Count + " Items were returned that weren't ELECTRONIC");
            skyBoxInventory.RemoveAll(x => x.stockType != "ELECTRONIC");
            Messages.Add("Leaving " + skyBoxInventory.Count + " SkyBox Items");



            // get the 1Ticket stock
            _1TickOrders = await _1Ticket.Functions.GetOrders("api%2B6718138216215%401ticket.com", "787171004", DateTime.Now.AddDays(-31), DateTime.Now);
            Messages.Add(_1TickOrders.getorders.order.Count + " inventory items found in 1Ticket");

            // the list of skybox items to update
            List<UpdateData> skyboxItemsToUpdate = new List<UpdateData>();
            List<UpdateNotData> skyboxItemsNotToUpdate = new List<UpdateNotData>();

            // loop for every SkyBox Inventory Item
            foreach (SkyBox.Model.Inventory.Row skyBoxItem in skyBoxInventory)
            {
                // find the corresponding 1Ticket item
                string confirmationCode = GetConfirmationFromNotes(skyBoxItem.notes);

                if (confirmationCode != "")
                {
                    skyboxItemsToUpdate.Add(new UpdateData
                    {
                        confirmationCode = confirmationCode,
                        skyBoxItemId = skyBoxItem.id
                    });
                }
                else
                {
                    skyboxItemsNotToUpdate.Add(new UpdateNotData
                    {
                        confirmationCode = confirmationCode,
                        skyBoxItem = skyBoxItem
                    });
                }
            }

            Messages.Add(skyboxItemsToUpdate.Count + " have the correct confirmation number");

            // update everything
            ActionBlock<UpdateData> block = new ActionBlock<UpdateData>(
                user => UpdateInventory(user),
                new ExecutionDataflowBlockOptions { MaxDegreeOfParallelism = 5 });

            skyboxItemsToUpdate.ForEach(user => block.Post(user));

            block.Complete();
            await block.Completion;
        }
        private static async Task UpdateInventory(UpdateData updateData)
        {
            SkyBox.Model.Inventory.Row skyBoxInvetoryItem = skyBoxInventory.Where(x => x.id == updateData.skyBoxItemId).FirstOrDefault();

            // find out if any of the barcodes or PDFs are empty
            bool emptyBarcodes = skyBoxInvetoryItem.tickets.Any(x => x.barCode == null);
            bool emptyPDFs = skyBoxInvetoryItem.tickets.Any(x => x.fileName == null);

            // set the flag to indicate if the SkyBox inventory item needs updating
            bool inventoryItemUpdated = false;

            if (emptyBarcodes || emptyPDFs)
            {
                // pull out the 1Ticket details
                _1Ticket.Model.Orders.robojGetordersOrder _1TicketOrder = _1TickOrders.getorders.order.Where(x => x.data_purchase.confnum == updateData.confirmationCode).FirstOrDefault();

                if (_1TicketOrder != null)
                {
                    // call the API for the 1Ticket Barcode and PDF information
                    _1Ticket.Model.BarcodePDF.seats barcodePDFDetails = await _1Ticket.Functions.GetBarcodePDFDetails("api%2B6718138216215%401ticket.com", "787171004", _1TicketOrder.id);

                    if (barcodePDFDetails != null)
                    {
                        // loop for every ticket in the SkyBox inventory item
                        foreach (SkyBox.Model.Inventory.Ticket skyBoxTicket in skyBoxInvetoryItem.tickets)
                        {
                            // pull out the seat info
                            _1Ticket.Model.BarcodePDF.seatsSeat _1TicketSeat = barcodePDFDetails.seat.Where(x => x.seatnumber == skyBoxTicket.seatNumber.ToString()).FirstOrDefault();

                            if (_1TicketSeat != null)
                            {
                                if (_1TicketSeat.barcode != "")
                                {
                                    // update the barcode info
                                    skyBoxTicket.barCode = _1TicketSeat.barcode;
                                    // indicate that this inventory item needs updating in SkyBox
                                    inventoryItemUpdated = true;
                                }
                                if (_1TicketSeat.pdfurl != "")
                                {
                                    // if the PDF doesn't already exist, upload it
                                    if (skyBoxTicket.fileName == null)
                                    {
                                        byte[] pdfAsBytes = await DownloadPDF(_1TicketSeat.pdfurl);
                                        if (pdfAsBytes != null)
                                        {
                                            skyBoxTicket.base64FileBytes = Convert.ToBase64String(pdfAsBytes);
                                        }
                                    }
                                    // indicate that this inventory item needs updating in SkyBox
                                    inventoryItemUpdated = true;
                                }
                            }
                        }
                    }
                }
            }

            if (inventoryItemUpdated)
            {
                // convert the inventory item to a string
                string sendThis = JsonConvert.SerializeObject(skyBoxInvetoryItem);
                // update the item via the API
                await Helpers.Functions.ExecuteJsonSkyBox(new Uri("https://skybox.vividseats.com:443/services/inventory/" + skyBoxInvetoryItem.id), new HttpMethod("PUT"), sendThis);
                skyBoxInventoryThatWasUpdated.Add(skyBoxInvetoryItem);
            }
        }
        private static async Task<byte[]> DownloadPDF(string pdfUrl)
        {
            TaskCompletionSource<byte[]> tcs = new TaskCompletionSource<byte[]>();
            HttpWebRequest request = WebRequest.CreateHttp(pdfUrl);
            using (HttpWebResponse response = (HttpWebResponse)(await request.GetResponseAsync()))
            using (Stream stream = response.GetResponseStream())
            using (MemoryStream ms = new MemoryStream())
            {
                await stream.CopyToAsync(ms);
                tcs.SetResult(ms.ToArray());
                return await tcs.Task;
            }
        }
        private static string GetConfirmationFromNotes(string note)
        {
            int index = note.IndexOf('/');
            if (index > 0)
            {
                //return note.Substring(0, index);
                if (note.Substring(0, index).Contains("-"))
                {
                    return note.Substring(0, index);
                }
            };
            return "";
        }
        private class UpdateData
        {
            public string confirmationCode { get; set; }
            public int skyBoxItemId { get; set; }
        }
        private class UpdateNotData
        {
            public string confirmationCode { get; set; }
            public SkyBox.Model.Inventory.Row skyBoxItem { get; set; }
        }
    }
}
